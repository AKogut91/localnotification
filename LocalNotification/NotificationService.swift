//
//  NotificationService.swift
//  LocalNotification
//
//  Created by AlexanderKogut on 10/11/18.
//  Copyright © 2018 AlexanderKogut. All rights reserved.
//

import Foundation
import UserNotifications

class LocalNotificationsService {
    
    static var shared = LocalNotificationsService()
    
    private init() {}
    
    let center = UNUserNotificationCenter.current()

    ///MARK: - Check Permission
    func initNotificationSetupCheck() {
        center.requestAuthorization(options: [.alert, .sound, .badge])
        { (success, error) in
            if success {
                print("Permission Granted")
            } else {
                print("There was a problem!")
            }
        }
    }
    
    ///SHOW Notification with titel, subtitle, body
    func showNotification() {
    
        let alertTitel = "alertTITEL"
 
        let content = UNMutableNotificationContent()
        
        content.title = alertTitel
        content.subtitle = "subtitle"
        content.body = ""
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest.init(identifier: "notify-test", content: content, trigger: trigger)
        
        //adding the notification to notification center
        center.add(request, withCompletionHandler: nil)
        
    }
}
