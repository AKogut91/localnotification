//
//  ViewController.swift
//  LocalNotification
//
//  Created by AlexanderKogut on 10/11/18.
//  Copyright © 2018 AlexanderKogut. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        LocalNotificationsService.shared.showNotification()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

